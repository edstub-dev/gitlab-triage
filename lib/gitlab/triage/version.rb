# frozen_string_literal: true

module Gitlab
  module Triage
    VERSION = '1.27.0'
  end
end
